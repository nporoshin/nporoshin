# Ex.3
# Найти активные (поле discontinued) продукты из категории Condiments и Meat/Poultry,
# которых в продаже менее 100 единиц.
# Вывести наименование продуктов, кол-во единиц в продаже, имя контакта поставщика и его телефонный номер.

from typing import List, Any

from sqlalchemy import text

from hw_20.models import Product, Supplier, Category
from hw_20.queries import active_condiments_meat_poultry
from hw_20.session_manager import open_db_session


def active_products_condiments_meat_poultry() -> List[tuple[Any, Any, Any, Any]]:
    """Return list of active products categorized as Condiments and Meat/Poultry,
    that are less than 100 units on sale.
    Function returns list of tuples were in each tuple:

    tuple[0] is a product name;

    tuple[1] is a number of units on sale;

    tuple[2] is a name of supplier's contact;

    tuple[4] is supplier's contacts phone number.
    """
    with open_db_session() as session:
        return session.execute(text(active_condiments_meat_poultry)).fetchall()


def active_products_condiments_meat_poultry_orm() -> List[tuple[Any, Any, Any, Any]]:
    """Return list of active products categorized as Condiments and Meat/Poultry,
    that are less than 100 units on sale.
    Function returns list of tuples were in each tuple:

    tuple[0] is a product name;

    tuple[1] is a number of units on sale;

    tuple[2] is a name of supplier's contact;

    tuple[4] is supplier's contacts phone number.
    """
    with open_db_session() as session:
        query = (
            session.query(Product, Supplier)
            .join(Supplier, Product.supplier_id == Supplier.supplier_id)
            .join(Category, Product.category_id == Category.category_id)
            .filter(Product.discontinued == 0)
            .filter(Product.units_in_stock < 100)
            .filter(Category.category_name.in_(["Condiments", "Meat/Poultry"]))
        ).all()

        return [
            (
                column.Product.product_name,
                column.Product.units_in_stock,
                column.Supplier.contact_name,
                column.Supplier.phone,
            )
            for column in query
        ]
