# Ex.4
# Подсчитать количество товаров в каждой категории, вывести по убыванию.

from typing import List, Any

from sqlalchemy import text, func, desc

from hw_20.models import Product, Category
from hw_20.queries import count_of_products_in_categories
from hw_20.session_manager import open_db_session


def count_products_in_categories() -> List[tuple[Any, Any]]:
    """Return count of products in each category,
    sorted by products count in descending order.
    Function returns list of tuples were in each tuple:

    tuple[0] is a category name;

    tuple[1] is a count of products in the category.
    """
    with open_db_session() as session:
        return session.execute(text(count_of_products_in_categories)).fetchall()


def count_products_in_categories_orm() -> List[tuple[Any, Any]]:
    """Return count of products in each category,
    sorted by products count in descending order.
    Function returns list of tuples were in each tuple:

    tuple[0] is a category name;

    tuple[1] is a count of products in the category.
    """
    with open_db_session() as session:
        return (
            session.query(
                Category.category_name,
                func.count(Product.product_name).label("Products count"),
            )
            .join(Category, Product.category_id == Category.category_id)
            .group_by(Category.category_name)
            .order_by(desc("Products count"))
            .all()
        )
