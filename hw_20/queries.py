customers_employees = """
            SELECT c.company_name "Company name",
            CONCAT(e.title_of_courtesy, e.first_name, ' ', e.last_name) AS "Full name"
            FROM customers c
                     JOIN orders o ON c.customer_id = o.customer_id
                     JOIN employees e ON o.employee_id = e.employee_id
                     JOIN shippers s ON o.ship_via = s.shipper_id
            WHERE c.city = 'London'
              AND e.city = 'London'
              AND s.company_name = 'Speedy Express'
"""

customers_no_orders = """
            SELECT c.company_name AS "Company name", c.customer_id AS "Customer ID"
            FROM customers c
                     LEFT JOIN orders o ON c.customer_id = o.customer_id
            WHERE o.customer_id IS NULL
"""
active_condiments_meat_poultry = """
            SELECT p.product_name   AS "Product name",
                   p.units_in_stock AS "In stock",
                   s.contact_name   AS "Contact name",
                   s.phone          AS "Phone number"
            FROM products p
                     JOIN suppliers s ON p.supplier_id = s.supplier_id
                     JOIN categories c ON p.category_id = c.category_id
            WHERE p.discontinued = 0
              AND p.units_in_stock < 100
              AND c.category_name IN ('Condiments', 'Meat/Poultry')
"""

count_of_products_in_categories = """
            SELECT c.category_name AS "Category", COUNT(p.product_name) AS "Products count"
            FROM products p
                     JOIN categories c ON p.category_id = c.category_id
            GROUP BY c.category_name
            ORDER BY "Products count" DESC
"""
