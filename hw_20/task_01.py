# Ex.1
# Найти заказчиков (customers) и обслуживающих их заказы сотрудников (employees).
# Заказчики и сотрудники из города London, доставка осуществляется компанией Speedy Express.
# Вывести компанию заказчика и ФИО сотрудника.

from typing import List, Any

from sqlalchemy import text

from hw_20.models import Customer, Order, Employee, Shipper
from hw_20.queries import customers_employees
from hw_20.session_manager import open_db_session


# RAW SQL
def custowers_with_employees() -> List[tuple[Any, Any]]:
    """Return list of customers and employees that service their orders.
    Function returns list of tuples were in each tuple:

    tuple[0] is a customer company name;

    tuple[1] is an employee's full name who services their orders.
    """
    with open_db_session() as session:
        return session.execute(text(customers_employees)).fetchall()


# ORM
def custowers_with_employees_orm() -> List[tuple[Any, Any]]:
    """Return list of customers and employees that service their orders.
    Function returns list of tuples were in each tuple:

    tuple[0] is a customer company name;

    tuple[1] is an employee's full name who services their orders.
    """
    with open_db_session() as session:
        query = (
            session.query(Customer, Employee)
            .join(Order, Customer.customer_id == Order.customer_id)
            .join(Employee, Order.employee_id == Employee.employee_id)
            .join(Shipper, Order.ship_via == Shipper.shipper_id)
            .filter(Customer.city == "London")
            .filter(Employee.city == "London")
            .filter(Shipper.company_name == "Speedy Express")
        ).all()

        return [
            (
                column.Customer.company_name,
                column.Employee.title_of_courtesy
                + column.Employee.first_name
                + " "
                + column.Employee.last_name,
            )
            for column in query
        ]
