from sqlalchemy import (
    Column,
    Date,
    Float,
    ForeignKey,
    Integer,
    LargeBinary,
    SmallInteger,
    String,
    Table,
    Text,
)
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from sqlalchemy.sql.sqltypes import VARCHAR

Base = declarative_base()
metadata = Base.metadata


class Category(Base):
    __tablename__ = "categories"

    category_id = Column(SmallInteger, primary_key=True)
    category_name = Column(String(15), nullable=False)
    description = Column(Text)
    picture = Column(LargeBinary)


class CustomerDemographic(Base):
    __tablename__ = "customer_demographics"

    customer_type_id = Column(VARCHAR, primary_key=True)
    customer_desc = Column(Text)


class Customer(Base):
    __tablename__ = "customers"

    customer_id = Column(VARCHAR, primary_key=True)
    company_name = Column(String(40), nullable=False)
    contact_name = Column(String(30))
    contact_title = Column(String(30))
    address = Column(String(60))
    city = Column(String(15))
    region = Column(String(15))
    postal_code = Column(String(10))
    country = Column(String(15))
    phone = Column(String(24))
    fax = Column(String(24))

    customer_types = relationship(
        "CustomerDemographic", secondary="customer_customer_demo"
    )

    def as_dict(self):
        return {
            "customer_id": self.customer_id,
            "company_name": self.company_name,
            "contact_name": self.contact_name,
            "contact_title": self.contact_title,
            "address": self.address,
            "city": self.city,
            "region": self.region,
            "postal_code": self.postal_code,
            "country": self.country,
            "phone": self.phone,
            "fax": self.fax,
        }


class Employee(Base):
    __tablename__ = "employees"

    employee_id = Column(SmallInteger, primary_key=True)
    last_name = Column(String(20), nullable=False)
    first_name = Column(String(10), nullable=False)
    title = Column(String(30))
    title_of_courtesy = Column(String(25))
    birth_date = Column(Date)
    hire_date = Column(Date)
    address = Column(String(60))
    city = Column(String(15))
    region = Column(String(15))
    postal_code = Column(String(10))
    country = Column(String(15))
    home_phone = Column(String(24))
    extension = Column(String(4))
    photo = Column(LargeBinary)
    notes = Column(Text)
    reports_to = Column(ForeignKey("employees.employee_id"))
    photo_path = Column(String(255))

    parent = relationship("Employee", remote_side=[employee_id])
    territorys = relationship("Territory", secondary="employee_territories")


class Region(Base):
    __tablename__ = "region"

    region_id = Column(SmallInteger, primary_key=True)
    region_description = Column(VARCHAR, nullable=False)


class Shipper(Base):
    __tablename__ = "shippers"

    shipper_id = Column(SmallInteger, primary_key=True)
    company_name = Column(String(40), nullable=False)
    phone = Column(String(24))


class Supplier(Base):
    __tablename__ = "suppliers"

    supplier_id = Column(SmallInteger, primary_key=True)
    company_name = Column(String(40), nullable=False)
    contact_name = Column(String(30))
    contact_title = Column(String(30))
    address = Column(String(60))
    city = Column(String(15))
    region = Column(String(15))
    postal_code = Column(String(10))
    country = Column(String(15))
    phone = Column(String(24))
    fax = Column(String(24))
    homepage = Column(Text)


class UsState(Base):
    __tablename__ = "us_states"

    state_id = Column(SmallInteger, primary_key=True)
    state_name = Column(String(100))
    state_abbr = Column(String(2))
    state_region = Column(String(50))


t_customer_customer_demo = Table(
    "customer_customer_demo",
    metadata,
    Column(
        "customer_id",
        ForeignKey("customers.customer_id"),
        primary_key=True,
        nullable=False,
    ),
    Column(
        "customer_type_id",
        ForeignKey("customer_demographics.customer_type_id"),
        primary_key=True,
        nullable=False,
    ),
)


class Order(Base):
    __tablename__ = "orders"

    order_id = Column(SmallInteger, primary_key=True)
    customer_id = Column(ForeignKey("customers.customer_id"))
    employee_id = Column(ForeignKey("employees.employee_id"))
    order_date = Column(Date)
    required_date = Column(Date)
    shipped_date = Column(Date)
    ship_via = Column(ForeignKey("shippers.shipper_id"))
    freight = Column(Float)
    ship_name = Column(String(40))
    ship_address = Column(String(60))
    ship_city = Column(String(15))
    ship_region = Column(String(15))
    ship_postal_code = Column(String(10))
    ship_country = Column(String(15))

    customer = relationship("Customer")
    employee = relationship("Employee")
    shipper = relationship("Shipper")


class Product(Base):
    __tablename__ = "products"

    product_id = Column(SmallInteger, primary_key=True)
    product_name = Column(String(40), nullable=False)
    supplier_id = Column(ForeignKey("suppliers.supplier_id"))
    category_id = Column(ForeignKey("categories.category_id"))
    quantity_per_unit = Column(String(20))
    unit_price = Column(Float)
    units_in_stock = Column(SmallInteger)
    units_on_order = Column(SmallInteger)
    reorder_level = Column(SmallInteger)
    discontinued = Column(Integer, nullable=False)

    category = relationship("Category")
    supplier = relationship("Supplier")


class Territory(Base):
    __tablename__ = "territories"

    territory_id = Column(String(20), primary_key=True)
    territory_description = Column(VARCHAR, nullable=False)
    region_id = Column(ForeignKey("region.region_id"), nullable=False)

    region = relationship("Region")


t_employee_territories = Table(
    "employee_territories",
    metadata,
    Column(
        "employee_id",
        ForeignKey("employees.employee_id"),
        primary_key=True,
        nullable=False,
    ),
    Column(
        "territory_id",
        ForeignKey("territories.territory_id"),
        primary_key=True,
        nullable=False,
    ),
)


class OrderDetail(Base):
    __tablename__ = "order_details"

    order_id = Column(ForeignKey("orders.order_id"), primary_key=True, nullable=False)
    product_id = Column(
        ForeignKey("products.product_id"), primary_key=True, nullable=False
    )
    unit_price = Column(Float, nullable=False)
    quantity = Column(SmallInteger, nullable=False)
    discount = Column(Float, nullable=False)

    order = relationship("Order")
    product = relationship("Product")
