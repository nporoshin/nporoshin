# Ex.2
# Найти заказчиков, у которых нет ни одного заказа. Вывести имя заказчика и customer_id.

from typing import List, Any

from sqlalchemy import text

from hw_20.models import Customer, Order
from hw_20.queries import customers_no_orders
from hw_20.session_manager import open_db_session


def customers_with_no_orders() -> List[tuple[Any, Any]]:
    """Return list of customers that doesn't have any orders.
    Function returns list of tuples were in each tuple:

    tuple[0] is a customer company name;

    tuple[1] is a customer ID.
    """
    with open_db_session() as session:
        return session.execute(text(customers_no_orders)).fetchall()


def customers_with_no_orders_orm() -> List[tuple[Any, Any]]:
    """Return list of customers that doesn't have any orders.
    Function returns list of tuples were in each tuple:

    tuple[0] is a customer company name;

    tuple[1] is a customer ID.
    """
    with open_db_session() as session:
        query = (
            session.query(Customer)
            .join(Order, Customer.customer_id == Order.customer_id, isouter=True)
            .filter(Order.customer_id == None)
        ).all()

        return [(column.company_name, column.customer_id) for column in query]
