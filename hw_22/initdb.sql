DROP TABLE IF EXISTS NumberInfo;

CREATE TABLE IF NOT EXISTS NumberInfo
(
    number INTEGER,
    info   TEXT,
    json   TEXT
);