# Ex.2
# Даны матрицы A[4, 6] и B[?x?]
# Каждая из них содержит экземпляры класса Shape из lesson 16. Найти результат умножения 2-ух матриц.

from random import randrange

from numpy import array

from hw_16.task_04 import Circle, Rectangle

my_circles = array([Circle(randrange(1, 101)) for _ in range(4) for _ in range(6)])
my_rectangles = array(
    [
        Rectangle(randrange(1, 101), randrange(1, 101))
        for _ in range(randrange(1, 11))
        for _ in range(randrange(1, 11))
    ]
)
try:
    matrix_a, matrix_b = array(my_circles).reshape(4, 6), array(my_rectangles).reshape(
        4, 6
    )
except ValueError:
    print("Error: arrays must be of the same size and shape in order to be multiplied")
else:
    matrix_c = matrix_a * matrix_b
