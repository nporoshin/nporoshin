# Ex.3
# Реализовать класс, который опрашивает ресурс http://numbersapi.com/
# Метод класса должен принимать любое число в соответствии с api и сохранять результат в локальном хранилище в формате:
# number: info
# Класс должен иметь возможность преобразовывать информацию в json.

# *в качестве локального хранилища использовать собственную базу. Схему и таблицы продумать самому.

from typing import Union

import requests as req
from sqlalchemy import text

from hw_22.session_manager import open_db_session


class NumbersInfo:
    API_URL = "http://numbersapi.com/{}/math/"

    def get_number_from_api(self, number: Union[int, str] = None) -> dict:
        """
        Query API_URL for some number and its fancy info.

        :param number: any number or string "random"
        :return: Response object in JSON format
        """
        if not number:
            number = "random"

        if number:
            self.API_URL = self.API_URL.format(number)

        response = req.get(url=self.API_URL + "?json").json()
        self._store_number_info(response)
        return response

    @staticmethod
    def _store_number_info(response: dict[str, str]):
        with open_db_session(with_commit=True) as session:
            session.execute(
                text(
                    f"""INSERT INTO NumberInfo (number, info, json) VALUES ({response["number"]}, $${response["text"]}$$, $${response}$$)"""
                )
            )

    @staticmethod
    def get_number_info(number: int, as_json: bool = False):
        with open_db_session() as session:
            if as_json:
                return session.execute(
                    text(
                        f"""SELECT DISTINCT REPLACE("json", '\"', '') FROM NumberInfo WHERE number = {number}"""
                    )
                ).fetchall()
        return session.execute(
            text(
                f"""SELECT DISTINCT number, info FROM NumberInfo WHERE number = {number}"""
            )
        ).fetchall()
