# Ex.1
# Даны 2 матрицы: A[5x5], B[5x5]. Каждая из них содержит экземпляры класса Shape из lesson 16.
# Найти результат сложения 2-ух матриц.

from random import randrange

from numpy import array

from hw_16.task_04 import Circle, Rectangle

my_circles = array([Circle(randrange(1, 101)) for _ in range(5) for _ in range(5)])
my_rectangles = array(
    [
        Rectangle((randrange(1, 101)), (randrange(1, 101)))
        for _ in range(5)
        for _ in range(5)
    ]
)
matrix_a, matrix_b = array(my_circles).reshape(5, 5), array(my_rectangles).reshape(5, 5)
matrix_c = matrix_a + matrix_b
