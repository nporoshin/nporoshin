# Ex. 1
# Написать функцию, которая определяет, является ли переданная строка палиндромом.
# Написать тесты, в т.ч. параметризированные.


def is_palindrome(word: str):
    return True if word.lower() == word[::-1].lower() else False
