# Ex.4
# Написать функцию, которая подсчитывает число вхождений символов в строке и выводит их в порядке убывания.
# Реализацию покрыть параметризированными тестами.
# Дополнительно написать тест, используя фикстуру из Ex.3


def count_each_symbol_in_string(string: str):
    symbol_counter = {}
    for symbol in string:
        symbol_counter[symbol] = string.count(symbol)

    return sorted(symbol_counter.items(), key=lambda x: x[1], reverse=True)


# Фикстура из Ex.3 исп-на в тесте test_count_each_symbol_in_string_function_using_fixture в tests/test_hw_21.py:105
