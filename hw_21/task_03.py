# Ex.3
# Написать фикстуру, которая генерирует строку случайных символов заданной длины.

from random import choices, choice
from string import ascii_lowercase, digits, punctuation

COMPLETE_SET_OF_SYMBOLS = ascii_lowercase + digits + punctuation


def generate_random_string(length: int = 42) -> str:
    if isinstance(length, float):
        length = round(length)

    if not length:
        raise ValueError("length could not be zero")

    elif length < 0:
        raise ValueError("length could not be negative number")

    elif length <= 3:
        return "".join(choices(population=COMPLETE_SET_OF_SYMBOLS, k=length))

    else:
        random_string = "".join(
            choice(ascii_lowercase) + choice(digits) + choice(punctuation)
        )

        while len(random_string) < length:
            random_string += choice(COMPLETE_SET_OF_SYMBOLS)

        return random_string
