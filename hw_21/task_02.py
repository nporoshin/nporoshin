# Ex.2
# Написать фикстуру, которая открывает соединение к тестовой базе данных и возвращает объект сессии/соединения.
# Из less_020 с помощью ORM записать в базу данные о книгах/издателях.
# Написать тест, который проверяет, что все записи были добавлены в БД.
# После выполнения теста соединение к БД должно быть закрыто в фикстуре.

from contextlib import contextmanager
from typing import ContextManager

from sqlalchemy import (
    MetaData,
    Table,
    Column,
    Integer,
    Text,
    ForeignKey,
    Date,
    create_engine,
    text,
)
from sqlalchemy.orm import Session, sessionmaker

from hw_21.models import Author, Publisher, Book
from hw_21.queries import alter_sequences_to_begin_with_1

engine = create_engine(
    "postgresql://nporoshin:asdf1234@localhost:7432/postgres"
).connect()
DBSession = sessionmaker(bind=engine)


@contextmanager
def open_db_session(with_commit=False) -> ContextManager[Session]:
    session = DBSession()
    try:
        yield session
        if with_commit:
            session.commit()
    except Exception:
        session.rollback()
        raise
    finally:
        session.close()


meta = MetaData()

Authors = Table(
    "author",
    meta,
    Column(
        "id",
        Integer,
        primary_key=True,
    ),
    Column("name", Text),
)

Publishers = Table(
    "publisher",
    meta,
    Column(
        "id",
        Integer,
        primary_key=True,
    ),
    Column("publisher_name", Text),
    Column("address", Text),
)

Books = Table(
    "book",
    meta,
    Column(
        "id",
        Integer,
        primary_key=True,
    ),
    Column("title", Text),
    Column("author_id", Integer, ForeignKey("author.id")),
    Column("date_published", Date),
    Column("isbn", Text),
    Column("publisher_id", Integer, ForeignKey("publisher.id")),
)

meta.create_all(bind=engine)
with open_db_session(with_commit=True) as session:
    session.execute(text(alter_sequences_to_begin_with_1))

authors_to_insert = [
    Author(name="James Patterson"),
    Author(name="Colleen Hoover"),
    Author(name="Delia Owens"),
    Author(name="James Clear"),
    Author(name="Alice Schertle"),
    Author(name="Taylor Jenkins Reid"),
    Author(name="Marilyn Sadler"),
    Author(name="Glenn Beck"),
    Author(name="Dr Seuss"),
]

publishers_to_insert = [
    Publisher(
        publisher_name="Hachette Book Group",
        address="1290 Avenue of the Americas, New York, NY 10104",
    ),
    Publisher(
        publisher_name="Simon & Schuster",
        address="1230 Avenue of the Americas, New York, NY 10020",
    ),
    Publisher(
        publisher_name="Penguin Group Usa",
        address="375 Hudson Street, New York, NY 10014",
    ),
    Publisher(
        publisher_name="Harpercollins Publishers",
        address="195 Broadway, New York, NY 10007",
    ),
    Publisher(
        publisher_name="Random House", address="1745 Broadway, New York, NY 10019"
    ),
]

books_to_insert = [
    Book(
        title="Run, Rose, Run",
        author_id=1,
        date_published="2022-03-07",
        isbn="9780759554375",
        publisher_id=1,
    ),
    Book(
        title="It Ends With Us",
        author_id=2,
        date_published="2016-08-02",
        isbn="9781982143657",
        publisher_id=2,
    ),
    Book(
        title="Where The Crawdads Sing",
        author_id=3,
        date_published="2021-03-30",
        isbn="9783446264199",
        publisher_id=3,
    ),
    Book(
        title="Atomic Habits",
        author_id=4,
        date_published="2021-03-30",
        isbn="9783442178582",
        publisher_id=3,
    ),
    Book(
        title="Verity",
        author_id=2,
        date_published="2021-10-26",
        isbn="9781408726600",
        publisher_id=1,
    ),
    Book(
        title="Little Blue Truck’s Springtime",
        author_id=5,
        date_published="2018-01-02",
        isbn="9781328466679",
        publisher_id=4,
    ),
    Book(
        title="The Seven Husbands Of Evelyn Hugo",
        author_id=6,
        date_published="2018-05-29",
        isbn="9788416517275",
        publisher_id=2,
    ),
    Book(
        title="It’s Not Easy Being A Bunny",
        author_id=7,
        date_published="1983-09-12",
        isbn="9780001711747",
        publisher_id=5,
    ),
    Book(
        title="The Great Reset",
        author_id=8,
        date_published="2022-01-11",
        isbn="9781637630594",
        publisher_id=2,
    ),
    Book(
        title="Green Eggs And Ham",
        author_id=9,
        date_published="1960-08-12",
        isbn="9781637630594",
        publisher_id=5,
    ),
]

with open_db_session(with_commit=True) as session:
    session.add_all(authors_to_insert)
    session.add_all(publishers_to_insert)
    session.add_all(books_to_insert)
