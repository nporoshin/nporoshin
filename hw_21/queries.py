create_table_author = """
CREATE TABLE IF NOT EXISTS Author
(
    id   SERIAL PRIMARY KEY,
    name TEXT
)
"""

create_table_publisher = """
CREATE TABLE IF NOT EXISTS Publisher
(
    id             SERIAL PRIMARY KEY,
    publisher_name TEXT,
    address        TEXT
)
"""

create_table_book = """
CREATE TABLE IF NOT EXISTS Book
(
    id             SERIAL PRIMARY KEY,
    title          TEXT,
    author_id      INTEGER,
    date_published DATE,
    isbn           TEXT,
    publisher_id   INTEGER,
    FOREIGN KEY (author_id) REFERENCES Author (id),
    FOREIGN KEY (publisher_id) REFERENCES Publisher (id)
)
"""

insert_into_author = """
INSERT INTO Author (name)
VALUES ('James Patterson'),
       ('Colleen Hoover'),
       ('Delia Owens'),
       ('James Clear'),
       ('Alice Schertle'),
       ('Taylor Jenkins Reid'),
       ('Marilyn Sadler'),
       ('Glenn Beck'),
       ('Dr Seuss')
"""

insert_into_publisher = """
INSERT INTO Publisher (publisher_name, address)
VALUES ('Hachette Book Group', '1290 Avenue of the Americas, New York, NY 10104'),
       ('Simon & Schuster', '1230 Avenue of the Americas, New York, NY 10020'),
       ('Penguin Group Usa', '375 Hudson Street, New York, NY 10014'),
       ('Harpercollins Publishers', '195 Broadway, New York, NY 10007'),
       ('Random House', '1745 Broadway, New York, NY 10019')
"""

insert_into_book = """
INSERT INTO Book (title, author_id, date_published, isbn, publisher_id)
VALUES ('Run, Rose, Run', 1, '2022-03-07', '9780759554375', 1),
       ('It Ends With Us', 2, '2016-08-02', '9781982143657', 2),
       ('Where The Crawdads Sing', 3, '2021-03-30', '9783446264199', 3),
       ('Atomic Habits', 4, '2021-03-30', '9783442178582', 3),
       ('Verity', 2, '2021-10-26', '9781408726600', 1),
       ('Little Blue Truck’s Springtime', 5, '2018-01-02', '9781328466679', 4),
       ('The Seven Husbands Of Evelyn Hugo', 6, '2018-05-29', '9788416517275', 2),
       ('It’s Not Easy Being A Bunny', 7, '1983-09-12', '9780001711747', 5),
       ('The Great Reset', 8, '2022-01-11', '9781637630594', 2),
       ('Green Eggs And Ham', 9, '1960-08-12', '9781637630594', 5)
"""

alter_sequences_to_begin_with_1 = """
ALTER SEQUENCE author_id_seq RESTART WITH 1;
ALTER SEQUENCE book_id_seq RESTART WITH 1;
ALTER SEQUENCE publisher_id_seq RESTART WITH 1;
"""
