from sqlalchemy import Column, Date, ForeignKey, Integer, Text, text
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship

Base = declarative_base()
metadata = Base.metadata


class Author(Base):
    __tablename__ = "author"

    id = Column(
        Integer,
        primary_key=True,
        server_default=text("nextval('author_id_seq'::regclass)"),
    )
    name = Column(Text)


class Publisher(Base):
    __tablename__ = "publisher"

    id = Column(
        Integer,
        primary_key=True,
        server_default=text("nextval('publisher_id_seq'::regclass)"),
    )
    publisher_name = Column(Text)
    address = Column(Text)


class Book(Base):
    __tablename__ = "book"

    id = Column(
        Integer,
        primary_key=True,
        server_default=text("nextval('book_id_seq'::regclass)"),
    )
    title = Column(Text)
    author_id = Column(ForeignKey("author.id"))
    date_published = Column(Date)
    isbn = Column(Text)
    publisher_id = Column(ForeignKey("publisher.id"))

    author = relationship("Author")
    publisher = relationship("Publisher")
