# Ex.2
# Задача
# Найти сумму всех чисел, меньших 1000, кратных 3 и 7.
# Реализовать через filter/map/reduce.

from functools import reduce


# ===== FILTER =====
def is_multiple_of_3_or_7(number: int):
    return number % 3 == 0 and number % 7 == 0


def _filter():
    filter_iterator = filter(is_multiple_of_3_or_7, range(1, 1001))
    sum_of_filtered_numbers = sum(filter_iterator)
    return sum_of_filtered_numbers


# ===== MAP =====
def multiple_of_3_or_7(number: int):
    return number if number % 3 == 0 and number % 7 == 0 else 0


def _map():
    map_iterator = map(multiple_of_3_or_7, range(1, 1001))
    sum_of_mapped_numbers = sum(map_iterator)
    return sum_of_mapped_numbers


# ===== REDUCE =====
def _reduce():
    numbers: list[int] = []
    for number in range(1, 1001):
        if number % 3 == 0 and number % 7 == 0:
            numbers.append(number)

    sum_of_numbers = reduce(lambda x, y: x + y, numbers)
    return sum_of_numbers
