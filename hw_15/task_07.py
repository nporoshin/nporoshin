# Ex.7
# Функция принимает список имен.
# Функция возвращает список отфильтрованных имен по переданному литералу.
#     :param
#         names: список имен
#         literal: буква фильтрации. Заглавный или строчный - не имеет значение.
#
#     :return:  список отфильтрованных имен. В ответе все имена должны начинаться с заглавной буквы и быть отсортированы
#
#     Example:
#     get_names_startswith_literal(['adam', 'Bob', 'Adrian'], 'a') == ['Adam', 'Adrian']

from typing import List


def filter_names_by_first_letter(names: List[str], literal: str) -> list:
    """Return a list of names that start with the supplied literal"""

    def sorting_helper(name: str):
        """Return True if first letter of the name equalls to the literal"""
        return name if name[0].lower() == literal.lower() else None

    return list(map(str.title, filter(sorting_helper, names)))
