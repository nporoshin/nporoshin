# Ex.6
# Реализовать класс-миксин, добавляющий классу Car атрибут spoiler.
# Spoiler должен влиять на Car.speed, увеличивая ее на значение N.

from hw_15.task_05 import Car


class SpoilerMixin:
    _has_spoiler = True

    @property
    def speed(self) -> int:
        return self._current_speed + 5

    CONSUMPTION_0_40 = (10 / 100) * 0.2
    CONSUMPTION_40_60 = (8 / 100) * 0.2
    CONSUMPTION_60_90 = (6 / 100) * 0.2
    CONSUMPTION_90_120 = (5.5 / 100) * 0.2
    CONSUMPTION_120_140 = (8 / 100) * 0.2
    CONSUMPTION_140_180 = (11 / 100) * 0.2


class CarWithSpoiler(SpoilerMixin, Car):
    pass
