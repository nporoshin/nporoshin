# Ex.5
# Реализовать класс Car в соответствии с интерфейсом.
# В нем реализовать метод speed, возвращающий текущую скорость.
# Реализовать декоратор метода. В случае превышения скорости — декоратор должен логировать в logger.error сообщение о
# превышении лимита.

import logging
import time
from functools import wraps
from typing import Callable

from hw_15.task_04 import ABCCar

logging.basicConfig(
    format="%(asctime)s %(message)s", datefmt="%d.%m.%Y|%H:%M:%S", level=logging.INFO
)
time = time.strftime("%d.%m.%Y|%H:%M:%S")

LOG: dict[str, str] = {}

SPEED_LIMIT: int = 80
LOG_MESSAGE = "Speed limit has been exceeded!"


def log_error_if_speed_limit_is_exceeded(func: Callable) -> Callable:
    @wraps(func)
    def wrapper(*args, **kwargs):
        result = func(*args, **kwargs)

        if result > SPEED_LIMIT:
            logging.error(LOG_MESSAGE)  # for running as a script
            LOG[time] = LOG_MESSAGE
            print(time, LOG_MESSAGE, sep=" ")  # for pytest

        return result

    return wrapper


class Car(ABCCar):
    def __init__(self, brand: str, car_body: str, color: str, max_speed: int):
        self._brand = brand
        self._car_body = car_body
        self._color = color
        self._max_speed = max_speed

        self._is_engine_working = False
        self._current_speed = 0

    def start_engine(self) -> str:
        self._is_engine_working = True
        return "Engine started"

    def stop_engine(self) -> str:
        self._is_engine_working = False
        return "Engine stopped"

    def increase_speed(self, value: int) -> str:
        if self._is_engine_working is True:
            if self.speed + value <= self._max_speed:
                self._current_speed += value
                return f"Now moving +{value} mph faster"
            else:
                return "Car is moving with the highest speed possible"
        else:
            return "Start the engine first"

    def decrease_speed(self, value: int) -> str:
        if self._is_engine_working is True:
            if self.speed - value >= 0:
                self._current_speed -= value
                return f"Now moving {value} mph slower"
            else:
                self._current_speed = 0
                return "Car has stopped"
        else:
            return "Start the engine first"

    @property
    @log_error_if_speed_limit_is_exceeded
    def speed(self) -> int:
        return self._current_speed
