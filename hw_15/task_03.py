# Ex.3
# Реализовать декоратор кэширования вызова функции.
# В случае, если вызывается функция c одинаковыми параметрами, то результат не должен заново вычисляться,
# а возвращаться из хранилища.
#
# * изучить lru_cache

from functools import wraps
from typing import Callable

cache: dict[int, int] = {}


def cache_func_calls(func: Callable) -> Callable:
    @wraps(func)
    def wrapper(n: int):

        if n in cache:
            return cache[n]

        result = func(n)

        if n not in cache:
            cache[n] = result

        return result

    return wrapper


@cache_func_calls
def fib(n: int):
    """Function to compute the Fibonacci number"""
    if n < 2:
        return n
    return fib(n - 1) + fib(n - 2)


if __name__ == "__main__":
    print(fib(10))
