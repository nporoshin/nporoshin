# Ex.4
# Реализовать интерфейс класса Car.

import abc


class ABCCar(abc.ABC):
    @abc.abstractmethod
    def start_engine(self):
        pass

    @abc.abstractmethod
    def stop_engine(self):
        pass

    @abc.abstractmethod
    def increase_speed(self, value):
        pass

    @abc.abstractmethod
    def decrease_speed(self, value):
        pass
