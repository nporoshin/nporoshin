# Ex.1
# Loops
# Написать цикл, который опрашивает пользователя и выводит рандомное число.
# Цикл должен прерываться по символу Q(q).

from random import randint


def ask_user_for_input(test_input: str = None):
    if test_input:
        ask_user = test_input
    else:
        ask_user = input("Enter something: ")

    return validate_input(ask_user)


def validate_input(_input: str):
    if _input.lower() == "q":
        return None
    else:
        return _input


def print_random_number_in_cycle(test_input: str = None):
    while True:
        if test_input is None:
            user_input = ask_user_for_input()
        elif test_input:
            user_input = ask_user_for_input(test_input)
            return user_input
        if user_input:
            print(randint(0, 100))
        if not user_input:
            break
