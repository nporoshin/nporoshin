create_table_departments = """
CREATE TABLE IF NOT EXISTS Departments (
id INTEGER PRIMARY KEY,
name TEXT UNIQUE,
manager_id INTEGER,
location_id INTEGER,
FOREIGN KEY(location_id) REFERENCES Location(id),
FOREIGN KEY(manager_id) REFERENCES Employees(id)
)
"""

create_table_employees = """
CREATE TABLE IF NOT EXISTS Employees (
id INTEGER PRIMARY KEY,
first_name TEXT,
last_name TEXT,
email TEXT UNIQUE,
phone_number TEXT UNIQUE,
hire_date TEXT,
job_id INTEGER,
manager_id INTEGER,
department_id INTEGER,
FOREIGN KEY(department_id) REFERENCES Departments(id)
)
"""

create_table_jobs = """
CREATE TABLE IF NOT EXISTS Jobs (
id INTEGER PRIMARY KEY,
title TEXT,
min_salary INTEGER,
max_salary INTEGER
)
"""

create_table_location = """
CREATE TABLE IF NOT EXISTS Location (
id INTEGER PRIMARY KEY,
street_address TEXT,
postal_code INTEGER,
city TEXT,
country_id INTEGER, 
FOREIGN KEY(country_id) REFERENCES Countries(id)
)
"""

create_table_countries = """
CREATE TABLE IF NOT EXISTS Countries (
id INTEGER PRIMARY KEY,
name TEXT UNIQUE
)
"""
