from sqlite3 import connect, IntegrityError
from typing import Union

import pytest

from hw_19.create_table_queries import *


class SQLiteDatabase:
    def __init__(self):
        self._connection = connect("example.db")
        self._cursor = self._connection.cursor()

    def __del__(self):
        self.close_connection()
        self._cursor = None
        self._connection = None

    def execute(self, query: str, with_response: bool = False, commit: bool = False):
        self._cursor.execute(query)
        if commit:
            self.commit()
        if with_response:
            return self.get_query_result()

    def get_query_result(self):
        return self._cursor.fetchall()

    def commit(self):
        self._connection.commit()

    def close_connection(self):
        try:
            self._cursor.close()
            self._connection.close()
        except:
            pass

    @staticmethod
    def normalize_response(items: list):
        return tuple(item[0] for item in items)

    def get_columns_of_table(self, table_name: str):
        columns = self.execute(
            f"SELECT name FROM pragma_table_info('{table_name}') WHERE name != 'id'",
            with_response=True,
        )
        if len(columns) == 1:
            return f"({columns[0][0]})"
        return self.normalize_response(columns)

    def insert_records(
        self,
        table_name: str,
        values: tuple,
        commit: bool = False,
    ):
        columns = self.get_columns_of_table(table_name)
        query_substring = ""
        for value in values:
            query_substring += "?, "
        query_substring = query_substring.removesuffix(", ")
        sql = f"INSERT INTO {table_name} {columns} VALUES ({query_substring})"
        query = self._cursor.execute(sql, values).fetchall()
        if commit:
            self._connection.commit()
        return query

    def update_records(
        self,
        table_name: str,
        column_name: str,
        value: Union[int, str, float],
        where: str,
        commit: bool = False,
    ):
        query = self._cursor.execute(
            f"UPDATE {table_name} SET {column_name} = {value} WHERE {where}"
        ).fetchall()
        if commit:
            self._connection.commit()
        return query

    def delete_records(
        self,
        table_name: str,
        where: str,
        commit: bool = False,
    ):
        query = self._cursor.execute(
            f"DELETE FROM {table_name} WHERE {where}"
        ).fetchall()
        if commit:
            self._connection.commit()
        return query

    def get_workers_count(self, department: str) -> int:
        return self._cursor.execute(
            f"""SELECT COUNT(*)
            FROM Employees e JOIN Departments d on e.department_id = d.id
            WHERE d.name = '{department}'
            """
        ).fetchall()[0][0]

    def get_managers_count(self) -> int:
        return self._cursor.execute(
            f"SELECT COUNT(*) FROM Employees e JOIN Jobs j on e.job_id = j.id WHERE j.title LIKE '%Manager%'"
        ).fetchall()[0][0]

    def get_average_salary(self) -> Union[int, float]:
        return self._cursor.execute(
            f"SELECT AVG((min_salary + max_salary) / 2) FROM Jobs j JOIN Employees e on j.id = e.job_id"
        ).fetchall()[0][0]

    def get_list_of_cities(self):
        query = self._cursor.execute(
            f"SELECT DISTINCT(city) FROM Location ORDER BY city"
        ).fetchall()
        return self.normalize_response(query)


@pytest.fixture
def connect_to_db():
    return SQLiteDatabase()


@pytest.fixture
def create_tables_in_db(connect_to_db):
    connect_to_db.execute(query=create_table_countries, commit=True)
    connect_to_db.execute(query=create_table_location, commit=True)
    connect_to_db.execute(query=create_table_jobs, commit=True)
    connect_to_db.execute(query=create_table_departments, commit=True)
    connect_to_db.execute(query=create_table_employees, commit=True)


@pytest.fixture
def populate_tables_with_sample_data(connect_to_db):
    try:
        connect_to_db.insert_records(
            table_name="Countries", values=("Belarus",), commit=True
        )
        connect_to_db.insert_records(
            table_name="Countries", values=("Poland",), commit=True
        )
        connect_to_db.insert_records(
            table_name="Countries", values=("Ukraine",), commit=True
        )
        connect_to_db.insert_records(
            table_name="Countries", values=("Portugal",), commit=True
        )

        connect_to_db.insert_records(
            table_name="Location",
            values=("some address 1", 12345, "Minsk", 1),
            commit=True,
        )
        connect_to_db.insert_records(
            table_name="Location",
            values=("some address 2", 67890, "Warsaw", 2),
            commit=True,
        )
        connect_to_db.insert_records(
            table_name="Location",
            values=("some address 3", 34567, "Kyiv", 3),
            commit=True,
        )
        connect_to_db.insert_records(
            table_name="Location",
            values=("some address 4", 45678, "Lisboa", 4),
            commit=True,
        )

        connect_to_db.insert_records(
            table_name="Jobs", values=("Software Architect", 10000, 20000), commit=True
        )
        connect_to_db.insert_records(
            table_name="Jobs", values=("Engineering Manager", 3500, 10000), commit=True
        )
        connect_to_db.insert_records(
            table_name="Jobs",
            values=("Senior Software Developer", 7000, 10000),
            commit=True,
        )
        connect_to_db.insert_records(
            table_name="Jobs",
            values=("Middle Software Developer", 4000, 7000),
            commit=True,
        )
        connect_to_db.insert_records(
            table_name="Jobs",
            values=("Junior Software Developer", 1000, 4000),
            commit=True,
        )
        connect_to_db.insert_records(
            table_name="Jobs", values=("Head of QA", 5000, 9000), commit=True
        )
        connect_to_db.insert_records(
            table_name="Jobs", values=("Senior QA Engineer", 3500, 5000), commit=True
        )
        connect_to_db.insert_records(
            table_name="Jobs", values=("Middle QA Engineer", 2000, 3500), commit=True
        )
        connect_to_db.insert_records(
            table_name="Jobs", values=("Junior QA Engineer", 1000, 2000), commit=True
        )

        connect_to_db.insert_records(
            table_name="Departments",
            values=("Product & Engineering", 1, 1),
            commit=True,
        )
        connect_to_db.insert_records(
            table_name="Departments", values=("Finance", 2, 2), commit=True
        )
        connect_to_db.insert_records(
            table_name="Departments", values=("Marketing", 3, 3), commit=True
        )
        connect_to_db.insert_records(
            table_name="Departments", values=("Operations", 4, 4), commit=True
        )

        connect_to_db.insert_records(
            table_name="Employees",
            values=(
                "Ivan",
                "Ivanov",
                "iivanov@test.com",
                375291234567,
                "01-01-2018",
                1,
                None,
                1,
            ),
            commit=True,
        )
        connect_to_db.insert_records(
            table_name="Employees",
            values=(
                "Sergey",
                "Sergeev",
                "ssergeev@test.com",
                375297654321,
                "01-01-2019",
                2,
                None,
                2,
            ),
            commit=True,
        )
        connect_to_db.insert_records(
            table_name="Employees",
            values=(
                "Ludmila",
                "Ludmilova",
                "lludmilova@test.com",
                375441234567,
                "01-01-2020",
                3,
                None,
                3,
            ),
            commit=True,
        )
        connect_to_db.insert_records(
            table_name="Employees",
            values=(
                "Svetlana",
                "Svetlanova",
                "ssvetlanova@test.com",
                375447654321,
                "01-01-2021",
                4,
                None,
                4,
            ),
            commit=True,
        )
        connect_to_db.insert_records(
            table_name="Employees",
            values=(
                "Pavel",
                "Pavlov",
                "ppavlov@test.com",
                375258674408,
                "24-01-2022",
                5,
                1,
                1,
            ),
            commit=True,
        )
        connect_to_db.insert_records(
            table_name="Employees",
            values=(
                "Anna",
                "Annova",
                "aannova@test.com",
                375258104476,
                "22-02-2022",
                6,
                2,
                2,
            ),
            commit=True,
        )
        connect_to_db.insert_records(
            table_name="Employees",
            values=(
                "Alena",
                "Alenova",
                "aalenova@test.com",
                375255960787,
                "11-03-2022",
                7,
                3,
                3,
            ),
            commit=True,
        )
        connect_to_db.insert_records(
            table_name="Employees",
            values=(
                "Maksim",
                "Maksimov",
                "mmaksimov@test.com",
                375259973458,
                "01-04-2022",
                8,
                4,
                4,
            ),
            commit=True,
        )
        connect_to_db.insert_records(
            table_name="Employees",
            values=(
                "Stepan",
                "Stepanov",
                "sstepanpv@test.com",
                375447849485,
                "11-04-2022",
                9,
                4,
                4,
            ),
            commit=True,
        )
    except IntegrityError:
        pass
