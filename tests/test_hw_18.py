from os.path import join

import pytest
from testfixtures import TempDirectory

from hw_18.task_01 import NamesIterable
from hw_18.task_02 import factorial_via_generator
from hw_18.task_03 import file_scanner


# task_01.py
def test_class_is_iterable():
    ni = NamesIterable(
        [
            "Ivanov Ivan Ivanovich",
            "Vladikov_Vladimir_Vadikovich",
            "Sergeev-Vlad-Stepanovich",
        ],
        "Vlad",
    )
    assert list(ni) == ["Sergeev-Vlad-Stepanovich"]


def test_empty_list():
    ni = NamesIterable([], "Vlad")
    assert list(ni) == []


def test_invalid_name():
    with pytest.raises(TypeError):
        ni = NamesIterable([])


# task_02.py
def test_type():
    gen = factorial_via_generator(1)
    assert type(gen).__name__ == "generator"


def test_factorial_calculation():
    import math

    gen = factorial_via_generator(5)
    assert next(gen) == 1
    assert next(gen) == 2
    assert next(gen) == 6
    assert next(gen) == 24
    assert next(gen) == math.factorial(5)


# task_03.py
def test_records_were_added_to_the_log():
    gen = file_scanner()
    temp_dir = TempDirectory()
    log_record_string = str(next(gen)).strip("()")
    log_file_path = join(temp_dir.path, "out.txt")
    temp_dir.write(log_file_path, log_record_string, encoding="UTF-8")
    assert temp_dir.read(log_file_path, encoding="UTF-8")
