from string import ascii_lowercase, digits, punctuation

import pytest

from hw_21.task_01 import is_palindrome
from hw_21.task_03 import COMPLETE_SET_OF_SYMBOLS
from hw_21.task_04 import count_each_symbol_in_string


# tests for task_01.py:
@pytest.mark.parametrize(
    "test_word,expected_result",
    [("refer", True), ("racecar", True), ("saippuakivikauppias", True)],
)
def test_positive_scenario(test_word, expected_result):
    assert is_palindrome(test_word) == expected_result


@pytest.mark.parametrize(
    "test_word,expected_result", [("cat", False), ("human", False), ("building", False)]
)
def test_negative_scenario(test_word, expected_result):
    assert is_palindrome(test_word) == expected_result


@pytest.mark.parametrize(
    "test_word,expected_result",
    [(42, False), (42.00, False), ({"smth": 42}, False), ([42], False)],
)
def test_exception_is_raised_if_word_is_not_string(test_word, expected_result):
    with pytest.raises(AttributeError):
        assert is_palindrome(test_word) == expected_result


# tests for task_02.py:
# def test_authors_were_inserted_into_db(db_connection):
#     assert db_connection.query(Author).count() == 9
#
#
# def test_publishers_were_inserted_into_db(db_connection):
#     assert db_connection.query(Publisher).count() == 5
#
#
# def test_books_were_inserted_into_db(db_connection):
#     assert db_connection.query(Book).count() == 10


# tests for task_03.py:
@pytest.mark.fixture_arg()
def test_generate_random_string_function(generate_random_string_fixture):
    assert len(generate_random_string_fixture) == 42


@pytest.mark.fixture_arg()
def test_generate_random_string_function_gt_3_symbols(generate_random_string_fixture):
    """Here I check that generated string contains at least one symbol from each string constants"""
    common_symbols = set(generate_random_string_fixture).intersection(ascii_lowercase)
    assert len(common_symbols) >= 1
    common_symbols = set(generate_random_string_fixture).intersection(digits)
    assert len(common_symbols) >= 1
    common_symbols = set(generate_random_string_fixture).intersection(punctuation)
    assert len(common_symbols) >= 1


@pytest.mark.fixture_arg(1)
def test_generate_random_string_function_that_is_eq_to_1_symbol(
    generate_random_string_fixture,
):
    assert generate_random_string_fixture in COMPLETE_SET_OF_SYMBOLS


@pytest.mark.fixture_arg(2)
def test_generate_random_string_function_that_is_eq_to_2_symbols(
    generate_random_string_fixture,
):
    assert generate_random_string_fixture[0] in COMPLETE_SET_OF_SYMBOLS
    assert generate_random_string_fixture[1] in COMPLETE_SET_OF_SYMBOLS


@pytest.mark.fixture_arg(3)
def test_generate_random_string_function_that_is_eq_to_3_symbols(
    generate_random_string_fixture,
):
    assert generate_random_string_fixture[0] in COMPLETE_SET_OF_SYMBOLS
    assert generate_random_string_fixture[1] in COMPLETE_SET_OF_SYMBOLS
    assert generate_random_string_fixture[2] in COMPLETE_SET_OF_SYMBOLS


@pytest.mark.fixture_arg(42.333)
def test_generate_random_string_function_float_lower_bound(
    generate_random_string_fixture,
):
    assert len(generate_random_string_fixture) == 42


@pytest.mark.fixture_arg(42.667)
def test_generate_random_string_function_float_upper_bound(
    generate_random_string_fixture,
):
    assert len(generate_random_string_fixture) == 43


@pytest.mark.fixture_arg(0)
@pytest.mark.xfail
def test_generate_random_string_function_0(generate_random_string_fixture):
    with pytest.raises(ValueError):
        raise


@pytest.mark.fixture_arg(-11.11)
@pytest.mark.xfail
def test_generate_random_string_function_negative(generate_random_string_fixture):
    with pytest.raises(ValueError):
        raise


# tests for task_04.py:
@pytest.mark.parametrize(
    "test_string,expected_result",
    [
        ("aaaaa", [("a", 5)]),
        ("55555", [("5", 5)]),
        ("%%%%%", [("%", 5)]),
        ("AAbbb42**", [("b", 3), ("A", 2), ("*", 2), ("4", 1), ("2", 1)]),
    ],
)
def test_count_each_symbol_in_string_function(test_string, expected_result):
    assert count_each_symbol_in_string(test_string) == expected_result


def test_count_each_symbol_in_string_function_using_fixture(
    generate_random_string_fixture,
):
    assert len(count_each_symbol_in_string(generate_random_string_fixture))
