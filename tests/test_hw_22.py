from numpy import array

# task_01.py
from hw_16.task_04 import Circle, Rectangle


def test_sum_of_matrices_a_b():
    circles = array([Circle(10) for _ in range(5) for _ in range(5)])
    rectangles = array([Rectangle(10, 15) for _ in range(5) for _ in range(5)])
    matrix_a, matrix_b = array(circles).reshape(5, 5), array(rectangles).reshape(5, 5)
    matrix_c = matrix_a + matrix_b
    assert matrix_c.all()


def test_sum_of_matrices_b_a():
    circles = array([Circle(11) for _ in range(5) for _ in range(5)])
    rectangles = array([Rectangle(15, 10) for _ in range(5) for _ in range(5)])
    matrix_a, matrix_b = array(circles).reshape(5, 5), array(rectangles).reshape(5, 5)
    matrix_c = matrix_b + matrix_a
    assert matrix_c.all()


# task_02.py
def test_mul_of_matrices_a_b():
    circles = array([Circle(5) for _ in range(4) for _ in range(6)])
    rectangles = array([Rectangle(5, 10) for _ in range(4) for _ in range(6)])
    matrix_a, matrix_b = array(circles).reshape(4, 6), array(rectangles).reshape(4, 6)
    matrix_c = matrix_a * matrix_b
    assert matrix_c.all()


def test_mul_of_matrices_b_a():
    circles = array([Circle(6) for _ in range(4) for _ in range(6)])
    rectangles = array([Rectangle(10, 15) for _ in range(4) for _ in range(6)])
    matrix_a, matrix_b = array(circles).reshape(4, 6), array(rectangles).reshape(4, 6)
    matrix_c = matrix_b * matrix_a
    assert matrix_c.all()


# task_03.py
