import pytest

from hw_15.task_01 import print_random_number_in_cycle
from hw_15.task_02 import _filter, _map, _reduce
from hw_15.task_03 import fib, cache
from hw_15.task_04 import ABCCar
from hw_15.task_05 import Car, LOG_MESSAGE, time, LOG
from hw_15.task_06 import CarWithSpoiler
from hw_15.task_07 import filter_names_by_first_letter


# task_01.py
def test_print_number_and_exit_from_cycle():
    assert (
        print_random_number_in_cycle(test_input="asdf1234") == "asdf1234"
    ), "Random number was not printed"
    assert (
        print_random_number_in_cycle(test_input="Q") is None
    ), "Cycle break on letter 'Q'(q) is not working"


# task_02.py
def test_sum_of_numbers_using_filter():
    assert _filter() == 23688


def test_sum_of_numbers_using_map():
    assert _map() == 23688


def test_sum_of_numbers_using_reduce():
    assert _reduce() == 23688


# task_03.py
def test_caching_of_function_params_and_results():
    fib(10)
    assert 10 in cache
    assert cache[10] == 55


# task_04.py
def test_cannot_create_abc_instance():
    with pytest.raises(TypeError):
        ABCCar()


# task_05.py
def test_log_error_if_speed_limit_exceeded():
    car = Car(brand="Toyota", car_body="sedan", color="Purple", max_speed=200)
    car.start_engine()
    car.increase_speed(80)
    assert car.speed == 80
    car.increase_speed(1)
    assert car.speed == 81
    assert time, LOG_MESSAGE in LOG.items()


# task_06.py
def test_car_speed_with_spoiler():
    car = CarWithSpoiler(
        brand="Toyota", car_body="sedan", color="Purple", max_speed=200
    )
    car.start_engine()
    car.increase_speed(25)
    assert car.speed == 30
    assert car._has_spoiler is True


# task_07.py
def test_returned_names_are_sorted_and_titlecased():
    assert filter_names_by_first_letter(["adam", "Bob", "Adrian"], "a") == [
        "Adam",
        "Adrian",
    ]
