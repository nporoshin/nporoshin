# from hw_20.task_01 import custowers_with_employees, custowers_with_employees_orm
# from hw_20.task_02 import customers_with_no_orders, customers_with_no_orders_orm
# from hw_20.task_03 import (
#     active_products_condiments_meat_poultry,
#     active_products_condiments_meat_poultry_orm,
# )
# from hw_20.task_04 import count_products_in_categories_orm, count_products_in_categories
#
#
# # task_01.py
# def test_query_returns_result():
#     assert (
#         custowers_with_employees().pop()
#         == custowers_with_employees_orm().pop()
#         == ("Seven Seas Imports", "Mr.Steven Buchanan")
#     )
#
#
# # task_02.py
# def test_query_returns_result():
#     assert (
#         customers_with_no_orders().pop()
#         == customers_with_no_orders_orm().pop()
#         == ("Paris spécialités", "PARIS")
#     )
#
#
# # task_03.py
# def test_query_returns_result():
#     assert (
#         active_products_condiments_meat_poultry().pop()
#         == active_products_condiments_meat_poultry_orm().pop()
#         == ("Original Frankfurter grüne Soße", 32, "Martin Bein", "(069) 992755")
#     )
#
#
# # task_04.py
# def test_query_returns_result():
#     assert (
#         count_products_in_categories().pop()
#         == count_products_in_categories_orm().pop()
#         == ("Produce", 5)
#     )
