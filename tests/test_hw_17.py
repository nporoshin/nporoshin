import pytest

from hw_17.task_01 import StopDrive, NewCarWithSpoiler, SpeedLimitError, FuelFinishError


# task_01.py
def test_fuel_finish_error_exception():
    test_car = NewCarWithSpoiler(
        brand="Toyota",
        car_body="sedan",
        color="Purple",
        max_speed=180,
        fuel_tank=1,
        distance_to_go=10000,
    )
    test_car.start_engine()
    test_car.one_hour_later()
    with pytest.raises(FuelFinishError):
        test_car.check_fuel_level()


def test_speed_limit_error_exception():
    test_car = NewCarWithSpoiler(
        brand="Toyota",
        car_body="sedan",
        color="Purple",
        max_speed=180,
        fuel_tank=100,
        distance_to_go=10000,
    )
    test_car.start_engine()
    test_car.one_hour_later()
    with pytest.raises(SpeedLimitError):
        test_car.check_speed_limit_signs(raise_immediately=True)


def test_stop_drive_exception():
    test_car = NewCarWithSpoiler(
        brand="Toyota",
        car_body="sedan",
        color="Purple",
        max_speed=180,
        fuel_tank=100,
        distance_to_go=1,
    )
    test_car.start_engine()
    test_car.one_hour_later()
    with pytest.raises(StopDrive):
        test_car.check_distance_passed()
