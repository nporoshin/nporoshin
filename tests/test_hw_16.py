from hw_16.task_01 import Parent, DirectChild, IndirectChild
from hw_16.task_02 import ClassToLog
from hw_16.task_03 import ShapeFabricMetaClass
from hw_16.task_04 import Rectangle, Triangle, Circle, ShapeInstancesContainer
from hw_16.task_05 import HostSettings


# task_01.py
def test_counter_changes_for_all_child_classes():
    parent = Parent()
    child = DirectChild()
    grandchild = IndirectChild()

    parent.increase_counter()
    child.increase_counter()
    grandchild.increase_counter()

    assert (parent.counter(), child.counter(), grandchild.counter()) == (3, 3, 3)


# task_02.py
def test_log_records_are_written_to_file():
    cwl = ClassToLog("test")
    cwl.get_attr()
    cwl.post_attr("test")
    cwl.delete_attr()
    with open("LOGGED_METHODS.txt") as file:
        record = file.readline()
        assert record


# task_03.py
def test_class_creation_based_on_shape_type():
    shape_types = ["Rectangle", "Triangle", "Circle"]

    for shape_type in shape_types:
        shape_class = ShapeFabricMetaClass(shape_type, (), {})
        assert "square" in shape_class.__dict__
        shape_class_object = shape_class()
        assert "square" in shape_class_object.__dir__()


def test_calculate_square_rectangle():
    Rectangle = ShapeFabricMetaClass("Rectangle", (), {})
    r = Rectangle()
    r.length, r.width = 10, 15
    assert r.square() == 150


def test_calculate_square_triangle():
    Triangle = ShapeFabricMetaClass("Triangle", (), {})
    t = Triangle()
    t.side_a, t.side_b, t.side_c = 10, 15, 12.5
    assert t.square() == 62.00979635307634


def test_calculate_square_circle():
    Circle = ShapeFabricMetaClass("Circle", (), {})
    c = Circle()
    c.radius = 10
    assert c.square() == 314.1592653589793


# task_04.py
def test_container_class_stores_figures():
    r, t, c = Rectangle(10, 15), Triangle(10, 15, 12.5), Circle(10)
    container = ShapeInstancesContainer(r, t, c)
    assert r in container.figures
    assert t in container.figures
    assert c in container.figures


def test_container_class_stores_squares():
    r, t, c = Rectangle(10, 15), Triangle(10, 15, 12.5), Circle(10)
    container = ShapeInstancesContainer(r, t, c)
    assert container.square == 526.1690617120557


# task_05.py
def test_dataclass_instance_contains_attributes():
    host = HostSettings(
        "192.168.0.1", 8080, "MacBook-Pro-15", "nporoshin@gmail.com", "", "google-oauth"
    )
    assert "ip_addr", "192.168.0.1" in host.__dict__.items()
    assert "port", 8080 in host.__dict__.items()
    assert "hostname", "MacBook-Pro-15" in host.__dict__.items()
    assert "username", "nporoshin@gmail.com" in host.__dict__.items()
    assert "password", "" in host.__dict__.items()
    assert "auth_type", "google-oauth" in host.__dict__.items()
