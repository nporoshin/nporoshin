# import os
# from sqlite3 import IntegrityError
#
# import pytest
#
#
# # task_01.py
# def test_prepare_test_data(
#     connect_to_db, create_tables_in_db, populate_tables_with_sample_data
# ):
#     assert os.path.exists("example.db")
#
#
# def test_get_workers_count_function(connect_to_db):
#     workers_count = connect_to_db.get_workers_count(department="Finance")
#     assert workers_count == 2
#
#
# def test_get_managers_count_function(connect_to_db):
#     managers_count = connect_to_db.get_managers_count()
#     assert managers_count == 1
#
#
# def test_get_average_salary_function(connect_to_db):
#     average_salary = connect_to_db.get_average_salary()
#     assert average_salary == 5972.222222222223
#
#
# def test_unique_constraint_exception(connect_to_db):
#     with pytest.raises(IntegrityError):
#         connect_to_db.insert_records(
#             table_name="Countries", values=("Belarus",), commit=True
#         )
