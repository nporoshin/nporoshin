# Ex.3
# Написать функцию, которая возвращает генератор, который сканирует папки/файлы и возвращает полный путь к файлам,
# размер которых больше 1 МБ: '/full_path_to_file', 'file_size'.
# Результат работы генератора записывается в файл out.txt.

from os import walk
from os.path import getsize
from os.path import join


def file_scanner(path: str = ".", file_size: int = 2**20):
    """
    :param path: Path to the folder where to begin searching
    :param file_size: Files smaller than file_size (in bytes) are ignored
    :return: A generator that yields files larger than file_size from the given path and all its subfolders
    """
    for dirpath, dirnames, filenames in walk(path):
        for filename in filenames:
            if getsize(join(dirpath, filename)) > file_size:
                yield f"{join(dirpath, filename)}", f"{getsize(join(dirpath, filename))}"
