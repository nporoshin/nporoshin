# Ex1
# Класс NamesIterable принимает коллекцию [Фамилия Имя Отчество].
# Реализовать итератор, который будет возвращать элементы, ориентируясь на Имя.
# Разделителями могут быть: ' ', '_', '-'.
#
# names = ["Ivanov Ivan Ivanovich", "Petrova-Olga-Sergeevna", "Petrichenko_Olga_Vladimirovna"]
#
# iterable = NamesIterable(names, 'Olga')
# iterator = iter(iterable)
# assert list(iterator) == ['Petrova Olga Sergeevna', 'Petrichenko_Olga_Vladimirovna']

import re
from typing import Iterable

ptrn = r"[\s\-_][a-z]+[\s\-_]"  # любое кол-во символов от a до z, включающее в себя с обеих сторон заданные разделители


class NamesIterable:
    def __init__(self, collection: Iterable[str], name: str):
        self._collection = collection
        self._name = name
        self._cursor = -1

    def __iter__(self):
        # Оставляю в коллекции только те строки, которые соответствуют паттерну. Перед сравнением переданного в
        # класс имени с паттерном, я обрезаю у строки, найденной по паттерну, заданные разделители
        self._collection = [
            item
            for item in self._collection
            if self._name
            == re.search(ptrn, item, flags=re.IGNORECASE).group().strip(" _-")
        ]
        return self

    def __next__(self):
        if self._cursor + 1 >= len(self._collection):
            raise StopIteration
        self._cursor += 1
        return self._collection[self._cursor]
