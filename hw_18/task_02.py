# Ex2
# Реализовать вычисление факториала через генератор.


def factorial_via_generator(number):
    sequence = (n for n in range(1, number + 1))
    value = 1
    while True:
        try:
            next_value = next(sequence)
            value *= next_value
            yield value
        except StopIteration:
            break
