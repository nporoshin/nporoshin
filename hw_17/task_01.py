# Ex. 1
# Из прошлых заданий доработать класс Car:
# — авто должен иметь топливный бак, рассчитанный на количество литров N.
# — при создании объекта машины бак полон и рассчитан на определенное количество км.
#
# При различных диапазонах скорости реализовать различный расход топлива:
# 0-40 км/ч == 10 литров/100 км
# 40-60 км/ч == 8 литров/100 км
# 60-90 км/ч == 6 литров/100 км
# 90-120 == 5.5 литров/100 км
# 120-140 == 8 литров/100 км
# 140-180 == 11 литров/100 км
#
# Реализовать ограничение скорости путем возбуждения исключения SpeedLimitError.
# Реализовать исключение FuelFinishError (при остатке хода = 0 км).
# Реализовать исключение StopDrive (окончание пути).
#
# Считать один проход цикла равным времени 1 час.
#
# Реализовать:
# — ввод расстояния L, которое необходимо проехать. Реализовать обработку ошибок невалидных данных
# — на каждую итерацию цикла происходит увеличение скорости на 5 км/ч. Авто начинает двигаться со скоростью 30 км/ч
# — рандомно возникают знаки ограничения скорости (диапазон пределов: 40, 60, 90, 120 км/ч)
# — при появления знака должно возбуждаться исключение SpeedLimitError и авто должен сбрасывать скорость до
# нужного предела (+ перерасчет скорости и расхода топлива)
# — при израсходовании топлива должно возбуждаться исключение FuelFinishError, после чего бак снова заполняется и авто
# начинает движение со скорости 30 км/ч
# — при прохождении всего расстояния должно возбуждаться исключение StopDrive - конец программы
# — логировать маршрут следования каждые 10 км
#
# * поддержать использование авто со спойлером (учесть расход +N литров и +S скорость км/ч)

from random import choice
from typing import Union

from hw_15.task_05 import Car
from hw_15.task_06 import SpoilerMixin

LOG: dict[str, str] = {}

STARTING_SPEED = 30


class SpeedLimitError(Exception):
    def __init__(self, speed_limit=None):
        print(f"Speed limit of {speed_limit} has been exceeded. Slowing down...")
        super().__init__(
            f"Speed limit of {speed_limit} has been exceeded. Slowing down..."
        )


class FuelFinishError(Exception):
    def __init__(self):
        print(f"Fuel tank has depleeted. Refilling...")
        super().__init__(f"Fuel tank has depleeted. Refilling...")


class StopDrive(Exception):
    def __init__(self):
        print(f"Engine has stopped — end of ride.")
        super().__init__(f"Engine has stopped — end of route.")


class NewCarWithSpoiler(SpoilerMixin, Car):
    CONSUMPTION_0_40 = 10 / 100
    CONSUMPTION_40_60 = 8 / 100
    CONSUMPTION_60_90 = 6 / 100
    CONSUMPTION_90_120 = 5.5 / 100
    CONSUMPTION_120_140 = 8 / 100
    CONSUMPTION_140_180 = 11 / 100

    def __init__(
        self,
        brand: str,
        car_body: str,
        color: str,
        max_speed: int,
        fuel_tank: Union[int, float],
        distance_to_go: Union[int, float],
    ):
        if not isinstance(distance_to_go, (int, float)):
            raise TypeError("distance_to_go must be a number")
        else:
            super().__init__(brand, car_body, color, max_speed)
            self.fuel_tank = fuel_tank
            self._fuel_tank_capacity = fuel_tank
            self.distance_to_go = distance_to_go

            self._current_speed = STARTING_SPEED
            self.last_speed_limit_sign = 0
            self.mileage = 0

    def one_hour_later(self):
        if 0 < self.speed <= 40:
            self.fuel_tank -= round(self.speed * self.CONSUMPTION_0_40, 2)
        elif 40 < self.speed <= 60:
            self.fuel_tank -= round(self.speed * self.CONSUMPTION_40_60, 2)
        elif 60 < self.speed <= 90:
            self.fuel_tank -= round(self.speed * self.CONSUMPTION_60_90, 2)
        elif 90 < self.speed <= 120:
            self.fuel_tank -= round(self.speed * self.CONSUMPTION_90_120, 2)
        elif 120 < self.speed <= 140:
            self.fuel_tank -= round(self.speed * self.CONSUMPTION_120_140, 2)
        elif 140 < self.speed <= 180:
            self.fuel_tank -= round(self.speed * self.CONSUMPTION_140_180, 2)

        self.distance_to_go -= self.speed
        self.mileage += self.speed
        self.increase_speed(5)
        self.log_route_every_10_km(hours_passed, self.mileage)

        return "One hour of driving has passed"

    def refill_fuel_tank(self) -> str:
        self.fuel_tank = self._fuel_tank_capacity
        return "Fuel tank has been refilled."

    def check_fuel_level(self):
        if self.fuel_tank <= 0:
            raise FuelFinishError

    def check_distance_passed(self):
        if self.distance_to_go <= 0:
            raise StopDrive

    def check_speed_limit_signs(self, raise_immediately: bool = None):
        if raise_immediately:
            raise SpeedLimitError()
        sign = choice([40, 60, 90, 120])
        if self.speed > sign:
            self.last_speed_limit_sign = sign
            raise SpeedLimitError(sign)

    def check_car_stats(self, hours_passed: int):
        print(
            f"{hours_passed} hour(s) passed. Current stats: current speed = {self.speed}, fuel tank = {self.fuel_tank},"
            f" remaining distance = {self.distance_to_go}"
        )

    @staticmethod
    def log_route_every_10_km(hours_passed: int, mileage: int):
        LOG[f"{hours_passed} hour(s) passed"] = f"The car went {mileage} kilometers."


car = NewCarWithSpoiler(
    brand="Toyota",
    car_body="sedan",
    color="Purple",
    max_speed=180,
    fuel_tank=100,
    distance_to_go=10000,
)

car.start_engine()

hours_passed = 1

while True:
    try:
        car.check_car_stats(hours_passed)
        car.check_distance_passed()
        car.check_fuel_level()
        car.check_speed_limit_signs()
        car.one_hour_later()

    except FuelFinishError:
        car.refill_fuel_tank()
        car._current_speed = STARTING_SPEED

    except SpeedLimitError:
        car._current_speed = car.last_speed_limit_sign

    except StopDrive:
        break

    finally:
        hours_passed += 1
