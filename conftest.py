import pytest

from hw_21.task_03 import generate_random_string


@pytest.fixture
def generate_random_string_fixture(request):
    marker = request.node.get_closest_marker("fixture_arg")
    if marker and marker.args:
        return generate_random_string(marker.args[0])
    else:
        return generate_random_string()
