# Ex.1
# Реализовать общий для всех классов counter и метод, который его изменяет на 1.
# Изменения должны быть видны всем дочерним классам.


class Parent:
    _counter = 0

    @staticmethod
    def increase_counter():
        Parent._counter += 1

    @staticmethod
    def counter():
        return Parent._counter


class DirectChild(Parent):
    pass


class IndirectChild(DirectChild):
    pass
