# Ex.5
# Реализовать дата-класс HostSettings. Должен иметь свойства ip_addr, port, hostname, username, password, auth_type.

from dataclasses import dataclass


@dataclass
class HostSettings:
    ip_addr: str
    port: int
    hostname: str
    username: str
    password: str
    auth_type: str
