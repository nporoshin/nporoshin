# Ex.2
# Реализовать класс, который имеет атрибут журнала логирования, куда попадают строки вида
# <дата-время вызова>-<класс>-<вызываемый метод>.

# * Реализовать декоратор, которым можно будет оборачивать тот метод, который мы захотим залогировать.

import logging

logger = logging.getLogger(__name__)
handler = logging.FileHandler("LOGGED_METHODS.txt")
handler.setLevel(logging.WARNING)
handler_format = logging.Formatter(
    fmt="<%(asctime)s>-<%(classname)s>-<%(msg)s>", datefmt="%d.%m.%Y-%H:%M:%S"
)
handler.setFormatter(handler_format)
logger.addHandler(handler)


def log_method_decorator(func):
    """Decorator that logs to file wrapped class method"""

    def wrapper(self, *args, **kwargs):
        result = func(self, *args, **kwargs)
        logger.warning(func.__name__, extra={"classname": self.__class__.__name__})
        return result

    return wrapper


class ClassToLog:
    def __init__(self, attr):
        self._attr = attr

    @log_method_decorator
    def get_attr(self) -> str:
        return self._attr

    @log_method_decorator
    def post_attr(self, attr: str):
        self._attr = attr

    @log_method_decorator
    def delete_attr(self):
        del self._attr
