# Ex.3
# Реализовать мета-класс фабрику, которая в зависимости от атрибута shape_type = ['Rectangle', 'Triangle', 'Circle']
# возвращает нужный класс с свойством square (площадь фигуры).

from math import pi
from typing import Union


def rectangle_square(self) -> Union[int, float]:
    return self.length * self.width


def triangle_square(self) -> Union[int, float]:
    p = (self.side_a + self.side_b + self.side_c) / 2
    area = (p * (p - self.side_a) * (p - self.side_b) * (p - self.side_c)) ** 0.5
    return area


def circle_square(self) -> Union[int, float]:
    return pi * self.radius**2


SHAPE_CLS_ATTRS = {
    "Rectangle": {"square": rectangle_square},
    "Triangle": {"square": triangle_square},
    "Circle": {"square": circle_square},
}


class ShapeFabricMetaClass(type):
    """This fabric creates different classes based on the supplied shape_type"""

    def __new__(mcs, shape_type: str, parents: tuple, attributes: dict):
        if shape_type in SHAPE_CLS_ATTRS:
            attributes.update(SHAPE_CLS_ATTRS[shape_type])
        return super().__new__(mcs, shape_type, parents, attributes)
