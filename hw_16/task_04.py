# Ex.4
# Класс-контейнер хранит экзепмляры класса Shape. Контейнер должен иметь свойство square,
# возвращающее общую площадь всех фигур.
# Реализациями Shape могут быть "Rectangle", "Triangle", "Circle". Shape должен предоставлять интерфейс square.
# Предусмотреть проверки.

from abc import ABC, abstractmethod
from math import pi
from typing import Union


class ABCShape(ABC):
    @abstractmethod
    def square(self):
        pass

    @abstractmethod
    def __add__(self, other):
        pass

    @abstractmethod
    def __mul__(self, other):
        pass


class Rectangle(ABCShape):
    def __init__(self, length: Union[int, float], width: Union[int, float]):
        self.length = length
        self.width = width

    @property
    def square(self) -> Union[int, float]:
        return self.length * self.width

    def __add__(self, other):
        return self.square + other.square

    def __mul__(self, other):
        return self.square * other.square


class Triangle(ABCShape):
    def __init__(
        self,
        side_a: Union[int, float],
        side_b: Union[int, float],
        side_c: Union[int, float],
    ):
        self.side_a = side_a
        self.side_b = side_b
        self.side_c = side_c

    @property
    def square(self) -> Union[int, float]:
        p = (self.side_a + self.side_b + self.side_c) / 2
        area = (p * (p - self.side_a) * (p - self.side_b) * (p - self.side_c)) ** 0.5
        return area

    def __add__(self, other):
        return self.square + other.square

    def __mul__(self, other):
        return self.square * other.square


class Circle(ABCShape):
    def __init__(self, radius: Union[int, float]):
        self.radius = radius

    @property
    def square(self) -> Union[int, float]:
        return pi * self.radius**2

    def __add__(self, other):
        return self.square + other.square

    def __mul__(self, other):
        return self.square * other.square


class ShapeInstancesContainer:
    """Stores instances of the Shape class"""

    def __init__(self, *args):
        self.figures = []
        for arg in args:
            self.figures.append(arg)

    @property
    def square(self):
        return sum((figure.square for figure in self.figures))
