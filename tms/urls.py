from django.urls import re_path

from .views import NumbersView, RangeView, HistoryView

urlpatterns = [
    re_path("^$", NumbersView.as_view(), name="index"),
    re_path(r"numbers/$", NumbersView.as_view(), name="number"),
    re_path(r"range/$", RangeView.as_view(), name="range"),
    re_path(r"range/(?P<length>\d*$)", RangeView.as_view(), name="range"),
    re_path(r"numbers/history/$", HistoryView.as_view(), name="history"),
]
