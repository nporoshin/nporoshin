import datetime

from django.views.generic import ListView
from django.views.generic.base import TemplateView

# Create your views here.
from .models import Numbers


class NumbersView(TemplateView):
    template_name = "tms/number.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        random_number = Numbers.generate_random_number()

        # goes into the template
        context["number"] = random_number

        # goes into the database
        Numbers.objects.create(
            datetime=datetime.datetime.now().isoformat(),
            url=self.request.path,
            result=random_number,
        )

        return context


class RangeView(TemplateView):
    template_name = "tms/range.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if "length" in self.kwargs:
            random_list = [
                Numbers.generate_random_number() for _ in range(int(kwargs["length"]))
            ]
        else:
            random_list = [Numbers.generate_random_number() for _ in range(10)]

        # goes into the template
        context["range"] = random_list

        # goes into the database
        Numbers.objects.create(
            datetime=datetime.datetime.now().isoformat(),
            url=self.request.path,
            result=random_list,
        )

        return context


class HistoryView(ListView):
    template_name = "tms/history.html"
    model = Numbers

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["numbers"] = Numbers.objects.all().order_by("-id")
        return context
