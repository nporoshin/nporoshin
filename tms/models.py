from random import randint

from django.db import models


# Create your models here.


class Numbers(models.Model):
    datetime = models.DateTimeField()
    url = models.CharField(max_length=100)
    result = models.TextField()

    @staticmethod
    def generate_random_number():
        return randint(0, 9223372036854775807)

    def __str__(self):
        return (
            str(self.datetime)
            + "|"
            + str(self.pk)
            + "|"
            + str(self.url)
            + "|"
            + str(self.result)
        )
